#!/usr/bin/perl -w
#
# Check "CVS-FILES" for "^[whitespace]$Id:" and insert "$Revision", "$Log", "$Date" 
# just after that.
#
# ra@br-online.de 2003-04-29
#
#

use File::Copy;

my $line = "";
my $filename = $ARGV[0];
my $tmpfilename = $filename.$$;
copy $filename => $tmpfilename or die "$!";

open (FILE,"< $tmpfilename");
open (OUTFILE,">$filename");

while (<FILE>) {
	if ($_ =~ /\$Id:/) {
		print OUTFILE "$_\n\t\$Date\$\n\t\$Revision\$\n\t\$Log\$\n"
	} else {
		print OUTFILE "$_"
	}
}
close(FILE);
close(OUTFILE);
system("rm $tmpfilename");

	
