#!/usr/bin/python

### v1: FOSDEM - Andreas Rogge (?)
### v2: LinuxWorld BE - Dag Wieers

### Applications to show (manual setup)
### - ws1: ooimpress (slide 1) + gnome-system-monitor
### - ws2: scribus (flyer) + inkscape (logo)
### - ws3: ooimpress (slide 2) + oowriter
### - ws4: evolution + calendar + sticky notes + firefox

from dogtail import *
import time, random

#print "startup"

### Gedit
#procedural.run('gedit')
#procedural.focus.application('gedit')
#procedural.click('Quit')

### Open Office presentation
#procedural.run('ooimpress')
#procedural.focus.application('ooimpress')

### Next workspace
#procedural.keyCombo('<Ctrl><Alt>Right')

### Sticky Notes
#procedural.focus.application('stickynotes_applet')
#procedural.click('New Note', roleName='menu item')
#procedural.click('', roleName='menu', raw=True, button = 3)
#procedural.click('Properties', roleName='menu item')
#procedural.focus.dialog('Sticky Note Properties')
#procedural.type("Don't forget")
#procedural.click('Close', roleName='push button')
#procedural.type("Work")
#procedural.keyCombo("Return")
#procedural.keyCombo("Return")

### Start on workspace 1
wsold = 0

try:
	while True:
		wsnew = random.randint(1, 3) % 4
		if wsnew < wsold:
			procedural.keyCombo('<Ctrl><Alt>Right')
			time.sleep(4) 
		elif wsnew > wsold:
			procedural.keyCombo('<Ctrl><Alt>Left')
			time.sleep(4) 
		else:
			wsold = 0
#			procedural.keyCombo('<Ctrl><Alt>Right<Ctrl><Alt>Right')
#			time.sleep(2)
			procedural.keyCombo('<Ctrl><Alt>Up<Ctrl><Alt>Right')
			time.sleep(2)
		wsold = wsnew
except:
	pass
#	print "cleanup"

	### Close Open Office
#	procedural.focus.application('ooimpress')
#	procedural.click('Quit')
